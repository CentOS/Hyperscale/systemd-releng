#!/bin/sh

package=$(basename $(git remote get-url origin) .git)
branch=$(git branch --show-current)

echo "WARNING: this script is deprecated"
echo "It was only needed when we had an old centpkg-minimal tagged"
echo
echo "Please use lookaside_upload_sig for Fedora-style dist-git repos with 'sources' file;"
echo "see https://sigs.centos.org/guide/git/"
echo "If you really want to continue, press ENTER, otherwise, use CTRL-C to abort"
read

while IFS= read -r line; do
  fname=$(echo "$line" | sed 's/SHA512 (\(.*\)) = \(.*\)/\1/g')
  digest=$(echo "$line" | sed 's/SHA512 (\(.*\)) = \(.*\)/\2/g')

  curl https://git.centos.org/sources/upload.cgi \
    --fail \
    --cert ~/.centos.cert \
    --form "name=${package}" \
    --form "branch=${branch}" \
    --form "sha512sum=${digest}" \
    --form "file=@${fname}" \
    --progress-bar
done < sources
