#!/bin/bash

set -eux -o pipefail

###############################################################################
#
# Script to create a new release branch or check out an existing release branch
# in the Hyperscale systemd Git repo.
#
###############################################################################

SYSTEMD_GIT_PATH=${SYSTEMD_GIT_PATH:-"$HOME/local/systemd-hs"}
GIT_REMOTE=${GIT_REMOTE:-origin}

if [[ "$#" -ne 1 ]]; then
    echo "Usage: $0 <systemd stable release version, e.g. 245.5>"
    exit 1
fi

if [[ ! -d "$SYSTEMD_GIT_PATH" ]]; then
    echo "You need to clone the Hyperscale systemd repo to $SYSTEMD_GIT_PATH"
    exit 1
fi

VERSION="$1"
UPSTREAM_TAG="v$VERSION"
HS_RELEASE_BRANCH="hs-v$VERSION"
FB_RELEASE_BRANCH="hs+fb-v$VERSION"
HS_BASE_RELEASE_COMMIT_TITLE="hs-systemd v$VERSION release base"

# Fetch all the branches, update the existing, and make sure there is not an
# existing release branch for this version
cd "$SYSTEMD_GIT_PATH"
git checkout main
git fetch -p
git pull --all
if git branch --all | grep -q "remotes/$GIT_REMOTE/$HS_RELEASE_BRANCH"; then
    echo "There is already a release branch for $VERSION! Checking out..."
    git checkout "$HS_RELEASE_BRANCH"
    git branch --set-upstream-to origin/"$HS_RELEASE_BRANCH"
    git pull
    exit 0
fi

# Add and pull down systemd-stable. We still use systemd-stable because it's
# easier than figuring out what commits to cherry pick ourselves (and all the
# major distros use it).
git remote add systemd-stable https://github.com/systemd/systemd-stable.git || true
git fetch systemd-stable

# Create the release branch
git checkout "$UPSTREAM_TAG" -b "$HS_RELEASE_BRANCH"

# Placeholder commit to mark where the Hyperscale patches start
git commit --allow-empty -m "$HS_BASE_RELEASE_COMMIT_TITLE"

# Create the release branch for FB specific backports
git checkout "$HS_RELEASE_BRANCH" -b "$FB_RELEASE_BRANCH"

# Push the branches out so everyone has them!
git push --set-upstream "$GIT_REMOTE" "$HS_RELEASE_BRANCH"
git push --set-upstream "$GIT_REMOTE" "$FB_RELEASE_BRANCH"

echo "Release branches $HS_RELEASE_BRANCH and $FB_RELEASE_BRANCH are ready! You can cherry-pick and apply patches now."
